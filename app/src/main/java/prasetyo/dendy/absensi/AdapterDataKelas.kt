package prasetyo.dendy.absensi

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView

class AdapterDataKelas (val dataKelas: List<HashMap<String,String>>,
                        val fragment: KelasFragment ) :
        RecyclerView.Adapter<AdapterDataKelas.HolderDataProdi>(){
    override fun onCreateViewHolder(
            p0: ViewGroup,
            p1: Int
    ): AdapterDataKelas.HolderDataProdi {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_kelas,p0,false)
        return HolderDataProdi(v)
    }

    override fun getItemCount(): Int {
        return dataKelas.size
    }

    override fun onBindViewHolder(p0: AdapterDataKelas.HolderDataProdi, p1: Int) {
        val data = dataKelas.get(p1)
        p0.txIdProdi.setText(data.get("id_kelas"))
        p0.txkelas.setText(data.get("nama_kelas"))
        p0.txsemester.setText(data.get("semester"))
        p0.txtingkat.setText(data.get("tingkat"))

        if (p1.rem(2) == 0) p0.cLayout2.setBackgroundColor(
                Color.rgb(230,245,240))
        else p0.cLayout2.setBackgroundColor(Color.rgb(255,255,245))



    }

    inner class HolderDataProdi(v : View) : RecyclerView.ViewHolder(v){
        val txIdProdi = v.findViewById<TextView>(R.id.txIdProdi)
        val txkelas = v.findViewById<TextView>(R.id.txkelas)
        val txsemester = v.findViewById<TextView>(R.id.txsemester)
        val txtingkat = v.findViewById<TextView>(R.id.txtingkat)
        val cLayout2 = v.findViewById<ConstraintLayout>(R.id.cLayout2)

    }

}


