package prasetyo.dendy.absensi

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray

class KelasFragment: Fragment() {
    lateinit var thisParent: AdminActivity
    lateinit var v : View

    lateinit var kelasAdapter : AdapterDataKelas
    var daftarKelas = mutableListOf<HashMap<String,String>>()
    val url = "http://192.168.9.108/absensi/show_data_kelas.php"


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as AdminActivity
        v =  inflater.inflate(R.layout.fragment_kelas, container, false)

        kelasAdapter = AdapterDataKelas(daftarKelas, this)
        var listMovie = v.findViewById<RecyclerView>(R.id.listKelas)
        listMovie.layoutManager = LinearLayoutManager(thisParent)
        listMovie.adapter = kelasAdapter


        val button = v.findViewById<Button>(R.id.btn_tambah)
        button.setOnClickListener {
            val intent = Intent(thisParent, TambahKelasActivity::class.java)
            startActivity(intent)
        }
        return v;
    }

    override fun onStart() {
        super.onStart()
        showDataKelas()
    }

    fun showDataKelas(){
        val request = StringRequest(
                Request.Method.POST,url,
                Response.Listener { response ->
                    daftarKelas.clear()
                    val jsonArray = JSONArray(response)
                    for(x in 0 .. (jsonArray.length()-1)){
                        val jsonObject = jsonArray.getJSONObject(x)
                        var kelas = HashMap<String,String>()
                        kelas.put("id_kelas",jsonObject.getString("id_kelas"))
                        kelas.put("nama_kelas",jsonObject.getString("nama_kelas"))
                        kelas.put("semester",jsonObject.getString("semester"))
                        kelas.put("tingkat",jsonObject.getString("tingkat"))
                        daftarKelas.add(kelas)
                    }
                    kelasAdapter.notifyDataSetChanged()
                },
                Response.ErrorListener { error ->
                    Toast.makeText(thisParent, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
                })
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }



}