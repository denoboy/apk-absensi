package prasetyo.dendy.absensi

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.navigation.NavigationView


class AdminActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    lateinit var drawer: DrawerLayout
    lateinit var toogle: ActionBarDrawerToggle

    lateinit var ft: FragmentTransaction
    lateinit var fragMessage: MessageFragment
    lateinit var fragChat: ChatFragment
    lateinit var fragProfile: ProfileFragment
    lateinit var fragKelas: KelasFragment

    lateinit var pake: Bundle
    lateinit var builder: AlertDialog.Builder

    lateinit var preferences: SharedPreferences
    val I = "setting"
    val U = "boolean"
    val k = "string"
    val def_u = "false"

    var p: String? = ""
    var t: String? = ""

    lateinit var toolbar: Toolbar
    lateinit var drawerLayout: DrawerLayout
    lateinit var navigationView: NavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin)

        fragMessage = MessageFragment()
        fragChat = ChatFragment()
        fragProfile = ProfileFragment()
        fragKelas = KelasFragment()
        pake = Bundle()

        toolbar = findViewById(R.id.toolbar)


        setSupportActionBar(toolbar)
        var drawer = findViewById(R.id.drawer_layout) as DrawerLayout
        drawerLayout = findViewById(R.id.drawer_layout)
        var nav = findViewById(R.id.nav_view) as NavigationView
        navigationView= findViewById(R.id.nav_view)


        navigationView.setNavigationItemSelectedListener(this)

        toogle = ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toogle)
        toogle.syncState()

        if (savedInstanceState == null) {
            ft = supportFragmentManager.beginTransaction()
            ft.replace(R.id.fragment_container, fragKelas).commit()
            navigationView.setCheckedItem(R.id.nav_kelas)
        }

        preferences = getSharedPreferences(I, Context.MODE_PRIVATE)
        p = preferences.getString(U, def_u)
//
//
        var paket: Bundle? = intent.extras
        var a = (paket?.getString("id"))
        t = preferences.getString(k, a)
//        Toast.makeText(this, t, Toast.LENGTH_SHORT).show()

        pake.putString("a", "jg")
        pake.putString("id", t)
        fragProfile.arguments = pake

        pake.putString("b", "jago")
        fragChat.arguments = pake
        fragKelas.arguments = pake

        builder = AlertDialog.Builder(this)

    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            val a = Intent(Intent.ACTION_MAIN)
            a.addCategory(Intent.CATEGORY_HOME)
            a.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(a)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_kelas -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.fragment_container, fragKelas).commit()
            }
            R.id.nav_dosen -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.fragment_container, fragChat).commit()
            }
            R.id.nav_matkul -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.fragment_container, fragProfile).commit()
            }
            R.id.nav_share -> {
                builder.setTitle("Konfirmasi").setMessage("Yakin logout ?")
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setPositiveButton("Ya", log)
                        .setNegativeButton("Tidak", null)
                builder.show()

            }
            R.id.nav_send -> {
                Toast.makeText(this, "Send Bro", Toast.LENGTH_SHORT).show()
            }
        }

        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    val log = DialogInterface.OnClickListener { dialog, which ->
        val prefEditor = preferences.edit()
        prefEditor.putString(U, "false")
        prefEditor.commit()
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }



}