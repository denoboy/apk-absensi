package prasetyo.dendy.absensi

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject


class LoginActivity : AppCompatActivity(){

    val url = "http://192.168.9.108/absensi/login.php"
    val h: Boolean = true
    lateinit var preferences: SharedPreferences
    val I = "setting"
    val U = "boolean"
    val k = "string"
    val def_u = "false"
    var p: String? = ""
    var ppp: String? = ""
    var pppp: String? = ""
    var pp: String? = ""
    lateinit var username: EditText
    lateinit var password: EditText


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        val button = findViewById(R.id.login) as Button
        username = findViewById(R.id.username)
        password = findViewById(R.id.inputsmtr)

        button.setOnClickListener {
            login("login")
        }

        preferences = getSharedPreferences(I, Context.MODE_PRIVATE)
        p = preferences.getString(U, def_u)
        pppp = preferences.getString(k, ppp)
        if (p == "true") {
            val intent = Intent(this, AdminActivity::class.java)
            intent.putExtra("id", pppp)
            startActivity(intent)
        }

    }
    var id: String = ""

    fun login(mode: String) {
        val request = object : StringRequest(
            Method.POST, url,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if (error.equals("000")) {
                    Toast.makeText(this, "Berhasil", Toast.LENGTH_SHORT).show()
                    var id = jsonObject.getString("id")
                    val prefEditor = preferences.edit()
                    prefEditor.putString(U, "true")
                    prefEditor.putString(k, id)
                    prefEditor.commit()


                    pp = id
                    Toast.makeText(this, id, Toast.LENGTH_SHORT).show()
                    val intent = Intent(this, AdminActivity::class.java)
                    intent.putExtra("id", id)
                    startActivity(intent)
                } else {
                    Toast.makeText(this, "Gagal", Toast.LENGTH_SHORT).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Tidak dapat tersambung ke server", Toast.LENGTH_SHORT).show()
            }
        ) {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                when (mode) {
                    "login" -> {
                        hm.put("mode", "login")
                        hm.put("username", username.text.toString())
                        hm.put("password", password.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
    override fun onBackPressed() {
        val a = Intent(Intent.ACTION_MAIN)
        a.addCategory(Intent.CATEGORY_HOME)
        a.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(a)
    }




}