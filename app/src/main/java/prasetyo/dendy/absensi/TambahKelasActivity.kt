package prasetyo.dendy.absensi

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.textfield.TextInputEditText
import org.json.JSONObject


class TambahKelasActivity : AppCompatActivity(){

    val url = "http://192.168.9.108/absensi/kelas.php"

    val arraySemester = arrayOf("1","2","3","4","5","6")
    lateinit var adapterSpin : ArrayAdapter <String>


    lateinit var nama: TextInputEditText
    lateinit var semester: Spinner
    lateinit var tingkat: TextInputEditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tambah_kelas)

        nama = findViewById(R.id.kelas)

        adapterSpin = ArrayAdapter(this, android.R.layout.simple_expandable_list_item_1,arraySemester)
        semester = findViewById(R.id.spinner)
        semester.adapter = adapterSpin

        semester.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(baseContext, "Tidak ada yg dipilih", Toast.LENGTH_SHORT).show()
            }

            override fun onItemSelected(p0: AdapterView<*>?, view: View?, position: Int, id: Long) {
//                Toast.makeText(baseContext, adapterSpin.getItem(position), Toast.LENGTH_SHORT).show()
            }
        }


        val button = findViewById(R.id.btntambah) as Button

        button.setOnClickListener {
            query("insert")
            onBackPressed()
        }

    }

    fun query(mode: String) {
        val request = object : StringRequest(
            Method.POST, url,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if (error.equals("000")) {
                    Toast.makeText(this, "Berhasil", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "Gagal", Toast.LENGTH_SHORT).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Tidak dapat tersambung ke server", Toast.LENGTH_SHORT).show()
            }
        ) {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                when (mode) {
                    "insert" -> {
                        hm.put("mode", "insert")
                        hm.put("nama", nama.text.toString())
                        hm.put("semester", adapterSpin.getItem(semester.selectedItemPosition).toString())
                        hm.put("tingkat", "I")
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}